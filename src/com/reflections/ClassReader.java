package com.reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ClassReader {

	public static void main(String[] args) {

		System.out.println("please enter the class name");
		Scanner sc = new Scanner(System.in);
		String className = sc.next();
		performClassReaderOperation(sc, className);
	}

	public static void performClassReaderOperation(Scanner sc, String className) {
		try {
			Class readClass = Class.forName(className);

			boolean loopCheck = true;
			do {
				System.out.println("Select the option you want to view");
				System.out.println("a. Methods");
				System.out.println("b. Class");
				System.out.println("c. Sub Classes");
				System.out.println("d. Parent Classes");
				System.out.println("e. Constructors");
				System.out.println("e. Data Members");
				String operation = sc.next();
				switch (operation) {
				case "a":
					Method[] methods = readClass.getMethods();
					List<String> methodList = new ArrayList<String>();
					for (int i = 0; i < methods.length; i++) {
						methodList.add(methods[i].toString());
						System.out.println(methods[i]);
					}
					loopCheck = postProcess(sc, methodList, "getMethods",readClass.getClass());
					break;
				case "b":
                    System.out.println(readClass.getName());
					break;
				case "c":
					System.out.println("There is no method to find subclass");
					break;
				case "d":
					Class superClass = readClass.getSuperclass();
					List<String> superClassList = new ArrayList<String>();
					superClassList.add(superClass.getName());
					System.out.println("Parent class: "+superClass.getName());
					loopCheck = postProcess(sc, superClassList, "getSuperClass",readClass.getClass());
					break;
				case "e":
					Constructor[] constructors = readClass.getConstructors();
					List<String> constructorList = new ArrayList<String>();
					for (int i = 0; i < constructors.length; i++) {
						constructorList.add(constructors[i].toString());
						System.out.println(constructors[i]);
					}
					loopCheck = postProcess(sc, constructorList, "getConstructors",readClass.getClass());
					break;
				case "f":
					Field[] dataMembers = readClass.getFields();
					List<String> dataMembersList = new ArrayList<String>();
					for (int i = 0; i < dataMembers.length; i++) {
						dataMembersList.add(dataMembers[i].toString());
						System.out.println(dataMembers[i]);
					}
					loopCheck = postProcess(sc, dataMembersList, "getFields",readClass.getClass());
					break;
				default:
					System.out.println("Wrong input....please enter again");
					break;
				}
			} while (loopCheck);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean postProcess(Scanner sc, List<String> operationList, String operation, Class className) {
		System.out.println("Do you want to see any other information");
		System.out.println(" Enter yes to recheck the menu and no to continue");
		String continueCheck = sc.next();
		if (continueCheck.equals("no")) {
			boolean continueOperation = true;
			do {
			System.out.println("1. Store information in the file");
			System.out.println("2. To see all the previous files created");
			System.out.println("3. Exit wihtout storing");
			int fileOperation = sc.nextInt();
			switch (fileOperation) {
			case 1:
				FileProcessing.fileWriter(operationList, operation,className);
				continueOperation=false;
				break;
			case 2:
				FileProcessing.fileReader();
				continueOperation=false;
				break;
			case 3:
				continueOperation=false;
				return false;
			default:
				System.out.println("Wrong option selected");
				break;
			}
			}while(continueOperation);
		} else if (continueCheck.equals("yes")) {
			return true;
		}
		return false;

	}
}
