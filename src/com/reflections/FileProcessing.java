package com.reflections;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class FileProcessing {

	public static void fileWriter(List<String> operationList, String operation, Class className) {
		String fileSuffix = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		try {
			FileWriter writer = new FileWriter("reflections_" + fileSuffix+".txt");
			writer.write(className +"--> Operation performed -->"+operation+"\n");
			for(String text:operationList) {
				writer.write(text+"\n");
			}
			writer.close();
				
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void fileReader() {
		File lastCreatedFile = getLastModified(System.getProperty("user.dir"));
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(lastCreatedFile));

			String st;
			while ((st = br.readLine()) != null)
				System.out.println(st);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static File getLastModified(String directoryFilePath) {
		File directory = new File(directoryFilePath);
		File[] files = directory.listFiles(File::isFile);
		long lastModifiedTime = Long.MIN_VALUE;
		File chosenFile = null;

		if (files != null) {
			for (File file : files) {
				if (file.lastModified() > lastModifiedTime) {
					chosenFile = file;
					lastModifiedTime = file.lastModified();
				}
			}
		}

		return chosenFile;
	}

}
